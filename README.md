**Develop branch status**: 
[![Build status](https://ci.appveyor.com/api/projects/status/65gkad90puso7gmr/branch/develop?svg=true)](https://ci.appveyor.com/project/Arash-Sabet/comingsoon/branch/develop)

**Project status:** 
[![Build status](https://ci.appveyor.com/api/projects/status/65gkad90puso7gmr?svg=true)](https://ci.appveyor.com/project/Arash-Sabet/comingsoon)

# ComingSoon - A small asp.net core app to boot landing pages for Startups

ComingSoon is a small asp.net core app that helps Startups to customize and publish their landing pages quickly. The landing page collects basic information like email address, company name and full name and saves those pieces of information in the application's SQL Server database. Also, an email is sent to notify the Startup upon signing up a prospect.

## Look and feel Customization

The project leverages [bootstrap](https://www.getbootstrap.com) to put a responsive web app in place. Following bootstrap's documentation is the easiest way to customize the look and feel of the UI per your brand.


## Deployment steps

Assuming setting up the DNS, IIS, etc. are all done separately, the following simple steps can be automated in a CI/CD pipeline:

- Deploy the database by executing the DAC package file generated after compiling the database project. Or you can create a deployment script to invoke the database creation script.
- Deploy the web app to your server or cloud. Make sure that the required .net core SDK or runtime is in place to support the application.
- Update database connection string and smtp server settings per your environment. The appsettings file looks like the following json payload with fictitious email server entries:

```json
"ConnectionStrings": {
    "ComsingSoonDatabase": "Data Source=(localdb)\\ProjectsV13;Initial Catalog=ComsingSoonDatabase;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"
  },
  "SmtpSettings": {
    "From": "hello@umplify.com",
    "SmtpServer": "smtpserver.umplify.com",
    "UserName": "smtpuser",
    "Password": "smtppassword"
  }
```
