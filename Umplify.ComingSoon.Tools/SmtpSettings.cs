﻿namespace Umplify.ComingSoon.Tools
{
    public sealed class SmtpSettings
    {
        public string From { get; set; }
        public string SmtpServer { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
