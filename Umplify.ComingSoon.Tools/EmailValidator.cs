﻿using FluentValidation;
using System;

namespace Umplify.ComingSoon.Tools
{
    public class EmailValidator : AbstractValidator<string>
    {
        public EmailValidator()
        {
            RuleFor(model => model)
                .Must(IsEmailAddressValid)
                .WithMessage("Email address is invalid.");
        }

        private bool IsEmailAddressValid(string email)
        {
            try
            {
                return string.Equals(new System.Net.Mail.MailAddress(email).Address, email, StringComparison.OrdinalIgnoreCase);
            }
            catch
            {
                return false;
            }
        }
    }
}
