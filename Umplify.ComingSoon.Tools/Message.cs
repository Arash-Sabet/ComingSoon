﻿using System.Collections.Generic;

namespace Umplify.ComingSoon.Tools
{
    public sealed class Message
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public List<string> To { get; } = new List<string>();
        public List<string> Cc { get; } = new List<string>();
        public List<string> Bcc { get; set; } = new List<string>();
    }
}
