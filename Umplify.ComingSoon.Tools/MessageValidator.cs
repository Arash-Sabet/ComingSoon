﻿using FluentValidation;
using System.Linq;

namespace Umplify.ComingSoon.Tools
{
    public class MessageValidator : AbstractValidator<Message>
    {
        public MessageValidator()
        {
            var emailValidator = new EmailValidator();

            RuleFor(message => message.To)
                .NotNull()
                .Must(to => to.Any());

            RuleFor(message => message.To).SetCollectionValidator(emailValidator);
            RuleFor(message => message.Cc).SetCollectionValidator(emailValidator);
            RuleFor(message => message.Bcc).SetCollectionValidator(emailValidator);
            RuleFor(message => message.Subject).NotEmpty();
            RuleFor(message => message.Body).NotEmpty();
        }
    }
}
