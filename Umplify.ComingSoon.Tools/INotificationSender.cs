﻿namespace Umplify.ComingSoon.Tools
{
    public interface INotificationSender
    {
        bool Send(Message message);
    }
}