﻿using FluentValidation;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace Umplify.ComingSoon.Tools
{
    public class EmailSender : INotificationSender
    {
        private readonly IValidator<Message> _messageValidator;
        private readonly SmtpSettings _smtpSettings;
        private readonly ILogger<EmailSender> _logger;
        public EmailSender(
            IValidator<Message> messageValidator,
            ILoggerFactory  loggerFactory,
            IOptions<SmtpSettings> smtpSettingsOptions)
        {
            _messageValidator = messageValidator;
            _smtpSettings = smtpSettingsOptions.Value;
            _logger = loggerFactory?.CreateLogger<EmailSender>();
        }

        public bool Send(Message message)
        {
            if (!_messageValidator.Validate(message).IsValid)
            {
                throw new Exception("Message object is invalid.");
            }

            try
            {
                var client = new SmtpClient(_smtpSettings.SmtpServer)
                {
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(_smtpSettings.UserName, _smtpSettings.Password)
                };

                client.Send(PrepareMailMessage(message));
                return true;
            }
            catch(Exception ex)
            {
                _logger?.LogError(ex.ToString());
            }

            return false;
        }

        private MailMessage PrepareMailMessage(Message message)
        {
            var mailMessage = new MailMessage
            {
                From = new MailAddress(_smtpSettings.From)
            };

            mailMessage.To.Add(message.To.Aggregate((c, n) => $"{c};{n}"));

            if (message.Cc.Any())
            {
                mailMessage.CC.Add(message.Cc.Aggregate((c, n) => $"{c};{n}"));
            }

            if (message.Bcc.Any())
            {
                mailMessage.Bcc.Add(message.Bcc.Aggregate((c, n) => $"{c};{n}"));
            }

            mailMessage.Body = message.Body;
            mailMessage.Subject = message.Subject;
            return mailMessage;
        }
    }
}
