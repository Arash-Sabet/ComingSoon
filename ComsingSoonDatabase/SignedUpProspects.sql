﻿CREATE TABLE [ComingSoon].[SignedUpProspects]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [EmailAddress] NVARCHAR(128) NOT NULL, 
    [CompanyName] NVARCHAR(256) NULL, 
    [PhoneNumber] NVARCHAR(32) NULL, 
    [FullName] NVARCHAR(100) NULL, 
    [CreatedDateTime] DATETIME NOT NULL, 
    [ClientIPAddress] NVARCHAR(15) NOT NULL, 
    [ReverseEmailAddress] NVARCHAR(128) NOT NULL
)

GO

CREATE UNIQUE INDEX [IX_SignedUpProspects_ReverseEmailAddress] ON [ComingSoon].[SignedUpProspects] ([ReverseEmailAddress])
