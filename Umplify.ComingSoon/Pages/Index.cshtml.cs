﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Umplify.ComingSoon.Abstractions;
using Umplify.ComingSoon.Tools;
using Umplify.ComngSoon.SqlRepository.Models;

namespace Umplify.ComingSoon.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IValidator<SignedUpProspects> _prospectValidator;
        private readonly IRepository<SignedUpProspects> _repository;
        private readonly INotificationSender _notificationSender;

        public IndexModel(
            INotificationSender notificationSender,
            IRepository<SignedUpProspects> repository,
            IValidator<SignedUpProspects> prospectValidator,
            ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory?.CreateLogger<IndexModel>();
            _prospectValidator = prospectValidator;
            _repository = repository;
            _notificationSender = notificationSender;
        }

        [BindProperty]
        public SignedUpProspects SignedUpProspects { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            SignedUpProspects.ClientIpaddress = HttpContext.Connection.RemoteIpAddress.ToString();
            var validationResult = _prospectValidator.Validate(SignedUpProspects);

            if (!validationResult.IsValid)
            {
                foreach(var error in validationResult.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.ErrorMessage);
                }

                return Page();
            }

            if (await _repository.SaveAsync(SignedUpProspects))
            {
                _notificationSender.Send(PrepareMessageToSend());
                return Redirect("/Confirm");
            }
            else
            {
                return Redirect("/Error");
            }
        }

        private Message PrepareMessageToSend()
        {
            var message = new Message();
            message.To.Add("info@umplify.com");
            message.Subject = "A new prospect left their email address";
            message.Body = $"Full name: {SignedUpProspects.FullName}\r\nEmail address:{SignedUpProspects.EmailAddress}\r\nCompany name:{SignedUpProspects.CompanyName}";
            return message;
        }

        public void OnGet()
        {
        }
    }
}
