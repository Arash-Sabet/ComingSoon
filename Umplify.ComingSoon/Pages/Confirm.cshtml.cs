﻿using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Umplify.ComingSoon.Pages
{
    public class ConfirmModel : PageModel
    {
        public string Message { get; set; }

        public void OnGet()
        {
            Message = "Thank you very much for giving us the opportunity to stay in touch with you when {Your Startup name} goes live!";
        }
    }
}
