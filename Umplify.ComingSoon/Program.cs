﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace Umplify.ComingSoon
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args)
                .Build()
                .Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
            => WebHost
            .CreateDefaultBuilder(args)
            .UseKestrel()
            .UseContentRoot(System.IO.Directory.GetCurrentDirectory())
            .ConfigureAppConfiguration((hostingContext,configBuilder) => 
            {
                configBuilder.SetBasePath(System.IO.Directory.GetCurrentDirectory());
                configBuilder
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: false)
                    .AddJsonFile($"appsettings.{hostingContext.HostingEnvironment.EnvironmentName}.json", optional: true, reloadOnChange: false);
                configBuilder.AddEnvironmentVariables();
            })
            .UseIISIntegration()
            .UseStartup<Startup>()
            .UseSerilog((hostingContext, loggerConfiguration) => 
            {
                loggerConfiguration
                    .WriteTo.File(@"..\Logs\Umplify.CommingSoon-.log",
                        rollingInterval: RollingInterval.Day,
                        rollOnFileSizeLimit: true,
                        fileSizeLimitBytes: 250_000_000,
                        retainedFileCountLimit: 31,
                        flushToDiskInterval: new System.TimeSpan(0,0,1),
                        buffered: true)
                   .MinimumLevel.Information()
                   .WriteTo.Console()
                   .MinimumLevel.Information();
            });
    }
}
