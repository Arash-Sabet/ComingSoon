﻿using Microsoft.Extensions.Configuration;
using Xunit.Abstractions;

namespace Umplify.ComingSoon.Tests
{
    public abstract class TestBase
    {
        protected readonly ITestOutputHelper _output;
        protected readonly string _connectionString;

        protected TestBase(ITestOutputHelper output)
        {
            _output = output;
            var config = new ConfigurationBuilder()
                .AddJsonFile("TestSettings.json")
                .Build();
            _connectionString = config.GetConnectionString("ComsingSoonDatabase");
        }
    }
}
