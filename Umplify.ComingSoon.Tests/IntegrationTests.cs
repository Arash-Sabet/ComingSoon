using System;
using System.Threading.Tasks;
using Umplify.ComingSoon.Abstractions;
using Umplify.ComngSoon.SqlRepository;
using Umplify.ComngSoon.SqlRepository.Models;
using Xunit;
using Xunit.Abstractions;

namespace Umplify.ComingSoon.Tests
{
    public class IntegrationTests : TestBase
    {
        private readonly IRepository<SignedUpProspects> _repository;

        public IntegrationTests(ITestOutputHelper output)
            : base(output) 
            => _repository = new Repository<SignedUpProspects>(_connectionString);

        [Fact]
        [Trait("Category","Integration Tests")]
        public async Task Should_Return_True_On_Adding_A_RecordsAsync()
        {
            var signedUpProspect = new SignedUpProspects
            {
                ClientIpaddress = "127.0.0.1",
                CompanyName = "Umplify Inc.",
                EmailAddress = "info@umplify.com",
                FullName = "John Smith",
                PhoneNumber = "416-111-2222"
            };

            var added = await _repository.SaveAsync(signedUpProspect);
            Assert.True(added);
        }
    }
}
