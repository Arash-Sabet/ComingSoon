﻿using Umplify.ComingSoon.Tools;
using Umplify.ComngSoon.SqlRepository.Models;
using Umplify.ComngSoon.SqlRepository.Validation;
using Xunit;
using Xunit.Abstractions;

namespace Umplify.ComingSoon.Tests
{
    public class UnitTests : TestBase
    {
        public UnitTests(ITestOutputHelper output) 
            : base(output)
        {
        }

        [Fact]
        [Trait("Category","Unit Tests")]
        public void Should_Return_True_On_Valid_Prospect()
        {
            var signedUpProspect = new SignedUpProspects
            {
                ClientIpaddress = "127.0.0.1",
                CompanyName = "Umplify Inc.",
                EmailAddress = "info@umplify.com",
                FullName = "John Smith",
                PhoneNumber = "416-111-2222"
            };

            var prospectValidator = new SignedUpProspectValidator();
            var validationResult = prospectValidator.Validate(signedUpProspect);
            Assert.True(validationResult.IsValid);
        }

        [Fact]
        [Trait("Category", "Unit Tests")]
        public void Should_Return_True_On_Valid_Prospect_Minimal_Info()
        {
            var signedUpProspect = new SignedUpProspects
            {
                ClientIpaddress = "127.0.0.1",
                CompanyName = "Umplify Inc.",
                EmailAddress = "info@umplify.com",
                FullName = "John Smith",
            };

            var prospectValidator = new SignedUpProspectValidator();
            var validationResult = prospectValidator.Validate(signedUpProspect);
            Assert.True(validationResult.IsValid);
        }

        [Fact]
        [Trait("Category", "Unit Tests")]
        public void Should_Return_False_On_Missing_Fullname_Prospect_Minimal_Info()
        {
            var signedUpProspect = new SignedUpProspects
            {
                ClientIpaddress = "127.0.0.1",
                CompanyName = "Umplify Inc.",
                EmailAddress = "info@umplify.com"
            };

            var prospectValidator = new SignedUpProspectValidator();
            var validationResult = prospectValidator.Validate(signedUpProspect);
            Assert.False(validationResult.IsValid);
        }

        [Fact]
        [Trait("Category", "Unit Tests")]
        public void Should_Return_False_On_Invalid_Prospect()
        {
            var signedUpProspect = new SignedUpProspects
            {
                CompanyName = "Umplify Inc.",
                EmailAddress = "info@umplify.com",
                FullName = "John Smith",
                PhoneNumber = "416-111-2222"
            };

            var prospectValidator = new SignedUpProspectValidator();
            var validationResult = prospectValidator.Validate(signedUpProspect);
            Assert.False(validationResult.IsValid);
        }

        [Fact]
        [Trait("Category", "Unit Tests")]
        public void Should_Return_True_On_Valid_Message1()
        {
            var message = new Message
            {
                Subject = "Subject line",
                Body = "This is the body of the message."
            };

            message.To.Add("click@umplify.com");
            var messageValidator = new MessageValidator();
            var validationResults = messageValidator.Validate(message);
            Assert.True(validationResults.IsValid);
        }

        [Fact]
        [Trait("Category", "Unit Tests")]
        public void Should_Return_True_On_Valid_Message2()
        {
            var message = new Message
            {
                Subject = "Subject line",
                Body = "This is the body of the message."
            };

            message.To.Add("click@umplify.com");
            message.To.Add("click2@umplify.com");
            var messageValidator = new MessageValidator();
            var validationResults = messageValidator.Validate(message);
            Assert.True(validationResults.IsValid);
        }

        [Fact]
        [Trait("Category", "Unit Tests")]
        public void Should_Return_True_On_Valid_Message3()
        {
            var message = new Message
            {
                Subject = "Subject line",
                Body = "This is the body of the message."
            };

            message.To.Add("click@umplify.com");
            message.Cc.Add("click2@umplify.com");
            message.Bcc.Add("click3@umplify.com");
            var messageValidator = new MessageValidator();
            var validationResults = messageValidator.Validate(message);
            Assert.True(validationResults.IsValid);
        }

        [Fact]
        [Trait("Category", "Unit Tests")]
        public void Should_Return_True_On_Inalid_Message1()
        {
            var message = new Message
            {
                Subject = "Subject line",
                Body = "This is the body of the message."
            };

            message.Cc.Add("click2@umplify.com");
            message.Bcc.Add("click3@umplify.com");
            var messageValidator = new MessageValidator();
            var validationResults = messageValidator.Validate(message);
            Assert.False(validationResults.IsValid);
        }

        [Fact]
        [Trait("Category", "Unit Tests")]
        public void Should_Return_True_On_Inalid_Message2()
        {
            var message = new Message
            {
                Subject = "Subject line",
                Body = "This is the body of the message."
            };

            message.Cc.Add("click2.umplify.com");
            message.Bcc.Add("click3@umplify.com");
            var messageValidator = new MessageValidator();
            var validationResults = messageValidator.Validate(message);
            Assert.False(validationResults.IsValid);
        }
    }
}
