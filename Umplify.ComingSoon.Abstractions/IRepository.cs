﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Umplify.ComingSoon.Abstractions
{
    public interface IRepository<T>
        where T : class
    {
        Task<bool> SaveAsync(T model);
        Task<bool> RemoveAsync(T model);
        Task<List<T>> GetAsync();
        void Close();
    }
}
