﻿using FluentValidation;
using System.Linq;
using Umplify.ComingSoon.Tools;
using Umplify.ComngSoon.SqlRepository.Models;

namespace Umplify.ComngSoon.SqlRepository.Validation
{
    public sealed class SignedUpProspectValidator : AbstractValidator<SignedUpProspects>
    {
        public SignedUpProspectValidator()
        {
            RuleFor(model => model.EmailAddress).SetValidator(new EmailValidator());

            RuleFor(model => model.ClientIpaddress)
                .NotEmpty()
                .MaximumLength(15);

            RuleFor(model => model.CompanyName)
                .NotEmpty()
                .MaximumLength(256);

            RuleFor(model => model.FullName)
                .NotEmpty()
                .MaximumLength(100)
                .Must(IsFullNameValid)
                .WithMessage("A person's full name has to have only alphabets.");

            RuleFor(model => model.CreatedDateTime)
                .NotNull();
        }

        private static bool IsFullNameValid(string fullname) 
            => !string.IsNullOrEmpty(fullname) && fullname.ToCharArray().All(ch => ch == ' ' || ch == '-' || char.IsLetter(ch));
    }
}
