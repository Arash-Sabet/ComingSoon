﻿using System;
using Umplify.ComngSoon.SqlRepository.Models;

namespace Umplify.ComngSoon.SqlRepository
{
    internal static class RepositoryHelper
    {
        public static string Reverse(this string input)
        {
            var array = input.ToCharArray();
            Array.Reverse(array);
            return new string(array);
        }
    }
}
