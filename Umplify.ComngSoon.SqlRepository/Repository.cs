﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Umplify.ComingSoon.Abstractions;
using Umplify.ComngSoon.SqlRepository.Models;

namespace Umplify.ComngSoon.SqlRepository
{
    public class Repository<T> : IRepository<T>
        where T : SignedUpProspects
    {
        private readonly ComsingSoonDatabaseContext _comsingSoonDatabaseContext;
        private readonly ILogger<Repository<T>> _logger;

        public Repository(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ComsingSoonDatabaseContext>();
            optionsBuilder.UseSqlServer(connectionString);
            _comsingSoonDatabaseContext = new ComsingSoonDatabaseContext(optionsBuilder.Options);
        }

        public Repository(ILoggerFactory loggerFactory,
            ComsingSoonDatabaseContext comsingSoonDatabaseContext)
        {
            _comsingSoonDatabaseContext = comsingSoonDatabaseContext;
            _logger = loggerFactory?.CreateLogger<Repository<T>>();
        }

        public Task<List<T>> GetAsync() 
            => throw new NotImplementedException();

        public Task<bool> RemoveAsync(T model) 
            => throw new NotImplementedException();

        public async Task<bool> SaveAsync(T model)
        {
            var saved = true;

            try
            {
                model.CreatedDateTime = DateTime.UtcNow;
                model.ReverseEmailAddress = model.EmailAddress.Reverse();
                await _comsingSoonDatabaseContext.AddAsync(model);
                await _comsingSoonDatabaseContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger?.LogCritical(ex.ToString());
                saved = false;
            }

            return saved;
        }

        public void Close()
            => _comsingSoonDatabaseContext.Database.CloseConnection();
    }
}
