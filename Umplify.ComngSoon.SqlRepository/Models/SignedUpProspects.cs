﻿using System;

namespace Umplify.ComngSoon.SqlRepository.Models
{
    public partial class SignedUpProspects
    {
        public int Id { get; set; }
        public string EmailAddress { get; set; }
        public string CompanyName { get; set; }
        public string PhoneNumber { get; set; }
        public string FullName { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ClientIpaddress { get; set; }
        public string ReverseEmailAddress { get; set; }
    }
}