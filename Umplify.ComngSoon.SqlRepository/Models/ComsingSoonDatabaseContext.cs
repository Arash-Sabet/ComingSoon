﻿using Microsoft.EntityFrameworkCore;

namespace Umplify.ComngSoon.SqlRepository.Models
{
    public partial class ComsingSoonDatabaseContext : DbContext
    {
        public ComsingSoonDatabaseContext()
        {
        }

        public ComsingSoonDatabaseContext(DbContextOptions<ComsingSoonDatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<SignedUpProspects> SignedUpProspects { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SignedUpProspects>(entity =>
            {
                entity.ToTable("SignedUpProspects", "ComingSoon");

                entity.HasIndex(e => e.ReverseEmailAddress)
                    .IsUnique();

                entity.Property(e => e.ClientIpaddress)
                    .IsRequired()
                    .HasColumnName("ClientIPAddress")
                    .HasMaxLength(15);

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.CreatedDateTime).HasColumnType("datetime");

                entity.Property(e => e.EmailAddress)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.PhoneNumber).HasMaxLength(32);

                entity.Property(e => e.ReverseEmailAddress)
                    .IsRequired()
                    .HasMaxLength(128);
            });
        }
    }
}